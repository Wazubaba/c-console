This library serves as a simple shim to support the standard 8 terminal
colors on both unix platforms via ansi escapes and windows via winapi calls.

Cygwin support is experimental though will work just fine if the detection
code works, in which case it will also emit the normal ansi escapes.

