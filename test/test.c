#include <console.h>
#include <stdio.h>

int main(int argc, char** argv)
{
	fg_set_red(0);
	puts("text");
	fg_set_blue(0);
	puts("text");
	fg_set_cyan(0);
	puts("text");
	fg_set_green(0);
	puts("text");
	fg_set_yellow(0);
	puts("text");
	fg_set_magenta(0);
	puts("text");
	fg_set_white(0);
	puts("text");
	fg_set_black(0);
	puts("text");
	reset();
	fg_set_red(1);
	puts("text");
	fg_set_blue(1);
	puts("text");
	fg_set_cyan(1);
	puts("text");
	fg_set_green(1);
	puts("text");
	fg_set_yellow(1);
	puts("text");
	fg_set_magenta(1);
	puts("text");
	fg_set_white(1);
	puts("text");
	fg_set_black(1);
	puts("text");
	fg_set_red(1);
	reset();
	puts("attributes reset");
	fg_set_red(0);
	puts("dull");
	fg_set_red(1);
	puts("bright");
	
	fg_set_red(0);
	bg_set_white(1);
	puts("dull red fg on bright white bg");
	reset();
	puts("and all settings cleared, take care and have fun \\o :D");
	return 0;
}
