#ifndef WINDOWS

#include "console.h"
#include <stdio.h>

inline void fg_set_black(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;30m");
	else
		printf("\033[1;30m");
}

inline void fg_set_red(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;31m");
	else
		printf("\033[1;31m");
}

inline void fg_set_green(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;32m");
	else
		printf("\033[1;32m");
}

inline void fg_set_yellow(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;33m");
	else
		printf("\033[1;33m");
}

inline void fg_set_blue(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;34m");
	else
		printf("\033[1;34m");
}

inline void fg_set_magenta(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;35m");
	else
		printf("\033[1;35m");
}

inline void fg_set_cyan(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;36m");
	else
		printf("\033[1;36m");
}

inline void fg_set_white(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;37m");
	else
		printf("\033[1;37m");
}

inline void bg_set_black(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;40m");
	else
		printf("\033[1;40m");
}

inline void bg_set_red(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;41m");
	else
		printf("\033[1;41m");
}

inline void bg_set_green(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;42m");
	else
		printf("\033[1;42m");
}

inline void bg_set_yellow(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;43m");
	else
		printf("\033[1;43m");
}

inline void bg_set_blue(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;44m");
	else
		printf("\033[1;44m");
}

inline void bg_set_magenta(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;45m");
	else
		printf("\033[1;45m");
}

inline void bg_set_cyan(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;46m");
	else
		printf("\033[1;46m");
}

inline void bg_set_white(const unsigned char bold)
{
	if (!bold)
		printf("\033[0;47m");
	else
		printf("\033[1;47m");
}

inline void reset()
{
	printf("\033[0m");
}

#endif

